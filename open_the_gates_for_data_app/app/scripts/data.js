

// Now, when the user selects a country, you will make a request for each dataset based on the country selected
function onSelect(event) {
  var selectedCountry = event.target.value;
  var countryCode = App._dataCountries[selectedCountry].id;
  // Request data for each one you want
  ajaxRequest(baseUrlPath + countryCode + '?downloadformat=csv', co2ChartData);
  
}


function ajaxRequest(url, successFunction) {
  $.ajax({
    url: url,
    success: successFunction,
    error: ajaxError
  });
}

// You will need to make one of these for each chart
function co2ChartData(data) {
  console.log('Raw data', data);
  // Do something with the CSV data here
  // I recommend using https://code.google.com/p/jquery-csv/
  // Very powerful tool that can convert the CSV rows to Objects or Arrays, etc
  // Fill in the chart
}

function ajaxError(jqXHR, textStatus, error) {
  console.error(jqXHR, textStatus, error);
}