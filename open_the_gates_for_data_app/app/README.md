New Media Design & Development I
================================

|Info|  |
|----|---|
|Olod|New Media Design & Development I|
|Auteur(s)|Simon de Poorter, Bram Van de Voorde|
|Opleiding|Bachelor in de Grafische en digitale media|
|Academiejaar|2015-16|
|ECTS-fiche|https://bamaflexweb.arteveldehs.be/BMFUIDetailxOLOD.aspx?a=47532&b=5&c=1|

***

Documenten
----------


Auteurs
--------

**Simon de Poorter**

* <http://github.com/simodepo>
* <http://bitbucket.org/simodepo>
* <simon.depoorter@student.arteveldehs.be>
	
**Bram Van de Voorde**

* <http://github.com/bramvand8>
* <http://bitbucket.org/bramvand8>
* <bram.vandevoorde1@student.arteveldehs.be>



Arteveldehogeschool
-------------------

- <http://www.arteveldehogeschool.be>
- <http://www.arteveldehogeschool.be/ects>
- <http://www.arteveldehogeschool.be/bachelor-de-grafische-en-digitale-media>
- <http://twitter.com/bachelorGDM>
- <https://www.facebook.com/BachelorGrafischeEnDigitaleMedia?fref=ts>


Copyright and license
---------------------

> Deze cursus mag niet gebruikt worden in andere opleidingen, hogescholen en universiteiten wereldwijd. Indien dit het geval zou zijn zullen jullie juridische stappen ondergaan.
>
> Code en documentatie copyright 2003-2016 [Arteveldehogeschool](http://www.arteveldehogeschool.be) | Opleiding Bachelor in de grafische en digitale media | drdynscript. Cursusmateriaal, zoals code, documenten, ... uitgebracht onder de [MIT licentie](LICENSE).